var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
	mix.copy('node_modules/bootstrap-sass/assets/fonts/', 'public/fonts');
	mix.scriptsIn('node_modules/bootstrap-sass/assets/javascripts/', 'public/js/vendor.js')
    mix.scriptsIn('node_modules/satellizer/', 'public/js/satellizer.js')
    mix.scriptsIn('resources/assets/js', 'public/js/app.js')
    mix.sass('app.scss');
    mix.version(["public/css/app.css", "public/js/vendor.js", "public/js/satellizer.js", "public/js/app.js"]);
});
