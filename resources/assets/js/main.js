(function() {

    'use strict';

    angular
        .module('dreamApp', ['ui.router', 'satellizer'])
        .config(function($stateProvider, $urlRouterProvider, $authProvider) {

            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = '/api/authenticate';

            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/auth');
            
            $stateProvider
                .state('auth', {
                    url: '/auth',
                    views:{
                        'login': {
                            templateUrl: '/templates/login.html',
                            controller: 'AuthController as auth'
                        },
                        'content': {
                            templateUrl: '/views/authView.html',
                            controller: 'AuthController as auth'
                        }
                    }
                    
                })
                .state('users', {
                    url: '/users',
                    views:{
                        'login': {
                            templateUrl: '/templates/login.html',
                            controller: 'AuthController as auth'
                        },
                        'content': {
                            templateUrl: '/views/userView.html',
                            controller: 'UserController as user'
                        }
                    }
                    
                });
        });
})();