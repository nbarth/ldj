<?php namespace App;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;
use Hash;

class UserTableSeeder extends Seeder {

  public function run()
  {
    DB::table('users')->delete();

        $users = array(
                ['name' => 'Nate Barth', 'email' => 'nathan.a.barth@gmail.com', 'password' => Hash::make('secret')],
                ['name' => 'Chris Sevilleja', 'email' => 'chris@scotch.io', 'password' => Hash::make('secret')],
                ['name' => 'Holly Lloyd', 'email' => 'holly@scotch.io', 'password' => Hash::make('secret')],
                ['name' => 'Adnan Kukic', 'email' => 'adnan@scotch.io', 'password' => Hash::make('secret')],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

  }
}

