(function() {

    'use strict';

    angular
        .module('dreamApp', ['ui.router', 'satellizer'])
        .config(function($stateProvider, $urlRouterProvider, $authProvider) {

            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = '/api/authenticate';

            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/auth');
            
            $stateProvider
                .state('auth', {
                    url: '/auth',
                    views:{
                        'login': {
                            templateUrl: '/templates/login.html',
                            controller: 'AuthController as auth'
                        },
                        'content': {
                            templateUrl: '/views/authView.html',
                            controller: 'AuthController as auth'
                        }
                    }
                    
                })
                .state('users', {
                    url: '/users',
                    views:{
                        'login': {
                            templateUrl: '/templates/login.html',
                            controller: 'AuthController as auth'
                        },
                        'content': {
                            templateUrl: '/views/userView.html',
                            controller: 'UserController as user'
                        }
                    }
                    
                });
        });
})();
(function() {

    'use strict';
    console.log('auth js ctl called');
    
    angular
        .module('dreamApp')
        .controller('AuthController', AuthController);


    function AuthController($auth, $state, $http, $rootScope) {

        var vm = this;

        vm.loginError = false;
        vm.loginErrorText;
            
        vm.login = function() {

            var credentials = {
                email: vm.email,
                password: vm.password
            }
            
            // Use Satellizer's $auth service to login
            $auth.login(credentials).then(function() {

                // Return an $http request for the now authenticated
                // user so that we can flatten the promise chain
                return $http.get('api/authenticate/user');

            // Handle errors
            }, function(error) {
                vm.loginError = true;
                vm.loginErrorText = error.data.error;

            // Because we returned the $http.get request in the $auth.login
            // promise, we can chain the next promise to the end here
            }).then(function(response) {

                // Stringify the returned data to prepare it
                // to go into local storage
                var user = JSON.stringify(response.data.user);

                // Set the stringified user data into local storage
                localStorage.setItem('user', user);

                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                $rootScope.authenticated = true;

                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app
                $rootScope.currentUser = response.data.user;

                // Everything worked out so we can now redirect to
                // the users state to view the data
                $state.go('users');
            });
        }

        vm.logout = function() {
            console.log('logout called');
            $auth.logout().then(function() {

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');

                // Flip authenticated to false so that we no longer
                // show UI elements dependant on the user being logged in
                $rootScope.authenticated = false;

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;

            });
        }
    }

})();
(function() {

    'use strict';
    console.log('user js ctl called');
    angular
        .module('dreamApp')
        .controller('UserController', UserController);  

    function UserController($http) {

        var vm = this;
        
        vm.users;
        vm.error;

        vm.getUsers = function() {
            console.log('get users called');
            // This request will hit the index method in the AuthenticateController
            // on the Laravel side and will return the list of users
            $http.get('api/authenticate').success(function(users) {
                vm.users = users;
            }).error(function(error) {
                vm.error = error;
            });
        }
    }
    
})();
//# sourceMappingURL=app.js.map